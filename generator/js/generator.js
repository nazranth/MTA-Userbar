$(document).ready(function() {
	$("#accName").change(function() {
		var username = $(this).val();
		var request = $.ajax ({
			type: "GET",
			data: {
				username: $(this).val()
			},
			url: "../api.php",
			dataType: 'json'
		});
		request.success (function(data) {
			var base64i = data.image;
			
			$("#sigImg").attr("src", "data:image/png;base64," + base64i);
			$("#forumBBcode").val("[img]http://gtirpg.net/MTA/userbar/signature.php?username=" + username + "[/img]");
			$("#signature").show();
			$("#error").show();
		});
		request.error (function() {
			$("#error").html("<p><b>Error:</b> Invalid user provided. (Do you have an account?)");
			$("#error").show();
		});
	});
});