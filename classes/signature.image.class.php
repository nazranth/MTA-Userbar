<?PHP
	class SignatureImg {
	
		//private $Backgrounds = Array("backgrounds/rockstar.png", "backgrounds/1.png", "backgrounds/building.png", "backgrounds/Diego.png", "backgrounds/explosion.png");
		private $Backgrounds;
		private $Font = array("Regular" => "../font/Roboto-Regular.ttf", "Bold" => "../font/Roboto-Bold.ttf");
		private $userdata;
		private $debug;
		
		public function __construct($userdata, $debug = false)
		{
			$this->userdata = $userdata; 
			$this->debug = $debug;
			$this->Backgrounds = $this->getAllBackgrounds();
			$this->Image  = imagecreatetruecolor(350, 125);
			imagefill($this->Image, 0, 0, imagecolorallocate($this->Image, 26, 26, 26));
			$this->Background = imagecreatefrompng($this->Backgrounds["explosion"]);
			
			imagecopy($this->Image, $this->Background, 0, 12, 0, 0, 400, 200);
			
			$this->Overlay = imagecreatefrompng("../images/required/overlay/overlay.png");
			imagecopy($this->Image, $this->Overlay, 0, 12, 0, 0, 400, 200);
			$this->doImageWriting();
			// END
		}
		
		public function Text($Text, $TextSize, $PositionX, $PositionY, $Bold = false, $Color = array(255, 255, 255), $px = 0)
		{
			if (!is_array($Color))
			{
				$px = $Color;
				$Color = array(255, 255, 255);
			}
			
			if ($px > 0)
			{
				for($c1 = ($PositionX-abs($px)); $c1 <= ($PositionX+abs($px)); $c1++)
				{
					for($c2 = ($PositionY-abs($px)); $c2 <= ($PositionY+abs($px)); $c2++)
					{
						$bg = imagettftext($this->Image, $TextSize, 0, $c1, $c2, imagecolorallocate($this->Image, 0, 0, 0), (($Bold) ? $this->Font["Bold"] : $this->Font["Regular"]), $Text);
					}
				}
			}
			imagettftext($this->Image, $TextSize, 0, $PositionX, $PositionY, imagecolorallocate($this->Image, $Color[0], $Color[1], $Color[2]), (($Bold) ? $this->Font["Bold"] : $this->Font["Regular"]), $Text);
		}
		
		public function getAllBackgrounds()
		{
			$imgs = array();
			$files = glob("../images/stock_backgrounds/*.png");
			foreach ($files as $file)
			{
				$fn = pathinfo($file, PATHINFO_FILENAME);
				$imgs[$fn] = $file;
			}
			return $imgs;
		}
		
		public function doImageWriting()
		{
			@$fontwidth = imagefontwidth($this->Font_B);
			$this->Text($this->userdata["lastname"], 8, ((imagesx($this->Overlay) / 2) - $fontwidth * strlen($this->userdata["lastname"]) / 2 - strlen($this->userdata["lastname"])), 10);
			$this->Text((($this->userdata["onlinestatus"] == "true") ? "Online" : "Offline"), 8, 3, 122.5, false, (($this->userdata["onlinestatus"] == "true") ? array(1, 223, 1) : array(255, 0, 0)));
			$startpoint = 27;
			$this->Text("Cash: \${$this->userdata["bank_bal"]}", 9, 25, $startpoint, true, 1);
			$this->Text("Kills: {$this->userdata["kills"]}", 9, 25, $startpoint + 25, true, array(255, 255, 255), 1);
			$this->Text("Deaths: {$this->userdata["deaths"]}", 9, 25, $startpoint + 50, true, 1);
			$this->Text("K/D ratio: {$this->userdata["kd_ratio"]}", 9, 25, $startpoint + 75, true, 1);
			$Location = $this->userdata["last_location"];
			$this->Text("Location: {$this->userdata["last_location"]}", 9, (imagesy($this->Overlay) +125 - max(strlen($Location), strlen("Random")) *3), $startpoint, true, 1);
			$this->Text("Group: {$this->userdata["group"]}", 9, (imagesy($this->Overlay) +125 - max(strlen($Location), strlen("Random")) *3), $startpoint + 25, true, 1);
			$this->Text("Team: {$this->userdata["team"]}", 9, (imagesy($this->Overlay) +125 - max(strlen($Location), strlen("Random")) *3), $startpoint + 50, true, 1);
			$this->Text("Last login: {$this->userdata["last_login"]}", 9, (imagesy($this->Overlay) +125 - max(strlen($Location), strlen("Random")) *3), $startpoint + 75, true, 1);
			$this->writeCopyright();
			
		}
		
		public function writeCopyright()
		{
			$this->Text("©Nathan", 8, (imagesx($this->Overlay) - 50), 10, false, array_fill(0, 3, 90));
			$this->Text("GTIRPG.net", 8, (imagesx($this->Overlay) - 62), 122.5);
		}
		
	public function imagettfstroketext(&$image, $size, $angle, $x, $y, &$textcolor, &$strokecolor, $fontfile, $text, $px) {
		for($c1 = ($x-abs($px)); $c1 <= ($x+abs($px)); $c1++)
			for($c2 = ($y-abs($px)); $c2 <= ($y+abs($px)); $c2++)
				$bg = imagettftext($image, $size, $angle, $c1, $c2, $strokecolor, $fontfile, $text);
			return imagettftext($image, $size, $angle, $x, $y, $textcolor, $fontfile, $text);
	}
		final public function AlphaBlend(){
			/* Prequel */
			@imagealphablending($this->Image, true);
			@imagealphablending($this->Background, true);
			@imagealphablending($this->Overlay, true);
			
			/* Sequel */
			@imagesavealpha($this->Image, true);
			@imagesavealpha($this->Background, true);
			@imagesavealpha($this->Overlay, true);
		}
		
		public function __destruct()
		{
			$this->AlphaBlend();
			if (isset($this->userdata["from_api"]))
			{
				ob_start();
					imagepng($this->Image);
					$imagedata = ob_get_contents();
				ob_end_clean();
				echo json_encode(array("image"=>base64_encode($imagedata)));
			}
			else
			{
				if (!$this->debug)
				{
					header("Content-type: image/png");
					imagepng($this->Image);
					imagedestroy($this->Image);
				}
			}
		}
		
	}
?>