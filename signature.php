<?PHP
if (isset($_GET['devv']))
{
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
}
date_default_timezone_set('Europe/London');
	
	if (!isset($_GET['username']))
	{
		die("No username specified.");
	}
	else {
		require_once("classes/signature.class.php");
		require_once("classes/signature.image.class.php");
		$data = new Sigs($_GET['username']);
		$img = new SignatureImg($data->userdata);
	}
	
?>