<?PHP

class Utils {
	public $wanted;
	
	public function Init()
	{
		if (!isset($_GET['username']))
		{
			die("No username specified.");
		}
		$this->sortWantedData();
	}
	
	public function sortWantedData()
	{
		$allowedData = array("bank", "team", "lastlogin", "name", "kills", "deaths", "group", "lastlocation", "playtime");
		$wanted = array();
		foreach ($_GET as $key => $val)
		{
			if ($key[0] == "s" && is_numeric($key[1]) && $key[1] < 8)
			{
				if (in_array($val, $allowedData))
				{
					$wanted[$key[1]] = $this->formatValueToKey($val);
				}
			}
			
			if ($key == "all")
			{
				for ($i = 0; $i < 8; $i++)
				{
					$wanted[$i] = $this->formatValueToKey($val);
				}
			}
			
			if ($key == "bg")
			{
				$wanted["bg"] = $val;
			}
		}
			
		for ($i = 0; $i < 8; $i++)
		{
			if (!isset($wanted[$i]))
			{
				$wanted[$i] = "random";
			}
		}
		$this->wanted = $wanted;
	}
	
	public function formatValueToKey($val)
	{
		switch($val)
		{
			case "bank":
				return "bank_bal";
			case "team":
				return "team";
			case "lastLogin":
				return "";
			case "name":
				return "name";
			case "kills":
				return "kills";
			case "deaths":
				return "deaths";
			case "group":
				return "group";
			case "lastlocation":
				return "last_location";
			case "playtime":
				return "playtime";
			case "jobrank":
				return "job_rank";
			default:
				return "random";
		}
	}
}



?>