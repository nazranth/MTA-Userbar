<?PHP
error_reporting(E_ALL);
ini_set('display_errors', '1');
date_default_timezone_set('Europe/London');
	class SignatureImg {
	
		//private $Backgrounds = Array("backgrounds/rockstar.png", "backgrounds/1.png", "backgrounds/building.png", "backgrounds/Diego.png", "backgrounds/explosion.png");
		private $Backgrounds;
/* 		private $Font_RG = "font/expressway.ttf";
		private $Font_B = "font/expressway_bold.ttf"; */
		private $Font = array("Regular" => "../font/expressway.ttf", "Bold" => "../font/expressway_bold.ttf");
		private $userdata;
		private $finaldata;
		
		public function __construct($userdata, $wanted)
		{
			$this->userdata = $userdata; // Array ( [bank_bal] => 61217318 [lastname] => LilDolla [onlinestatus] => false [id] => 1 [name] => LilDollaTechZone [kills] => 52 [deaths] => 66 [kd_ratio] => 0.79 )
			// ALL BELOW IS REQUIRED
			$this->wanted = $wanted;
			
			if (isset($this->userdata["from_api"]))
			{
				$this->api = true;
				unset($this->userdata["from_api"]);
			}
			$this->Backgrounds = $this->getAllBackgrounds();
			$this->Image  = imagecreatetruecolor(350, 125);
			imagefill($this->Image, 0, 0, imagecolorallocate($this->Image, 26, 26, 26));
			$this->Background = imagecreatefrompng(((strtolower($this->userdata["name"]) == "diego") ? $this->Backgrounds["police"] : $this->Backgrounds["explosion"]));
			
			imagecopy($this->Image, $this->Background, 0, 12, 0, 0, 400, 200);
			
			$this->Overlay = imagecreatefrompng("../images/required/overlay/overlay.png");
			imagecopy($this->Image, $this->Overlay, 0, 12, 0, 0, 400, 200);
			
			
			$this->writeRequiredContent();
			$this->gatherFinalData();
			$this->doImageWriting();
			
			
			
			// END
		}
		
		public function Text($Text, $TextSize, $PositionX, $PositionY, $Bold = false, $Color = array(255, 255, 255), $px = 0)
		{
			if (!is_array($Color))
			{
				$px = $Color;
				$Color = array(255, 255, 255);
			}
			
			if ($px > 0)
			{
				for($c1 = ($PositionX-abs($px)); $c1 <= ($PositionX+abs($px)); $c1++)
				{
					for($c2 = ($PositionY-abs($px)); $c2 <= ($PositionY+abs($px)); $c2++)
					{
						$bg = imagettftext($this->Image, $TextSize, 0, $c1, $c2, imagecolorallocate($this->Image, 0, 0, 0), (($Bold) ? $this->Font["Bold"] : $this->Font["Regular"]), $Text);
					}
				}
			}
			imagettftext($this->Image, $TextSize, 0, $PositionX, $PositionY, imagecolorallocate($this->Image, $Color[0], $Color[1], $Color[2]), (($Bold) ? $this->Font["Bold"] : $this->Font["Regular"]), $Text);
		}
		
		public function getAllBackgrounds()
		{
			$imgs = array();
			$files = glob("../images/stock_backgrounds/*.png");
			foreach ($files as $file)
			{
				$fn = pathinfo($file, PATHINFO_FILENAME);
				$imgs[$fn] = $file;
			}
			return $imgs;
		}
		
		public function doImageWriting()
		{
			$maxstr = $this->getLenOfLongestVal($this->finaldata);
			
			$i = 1;
			foreach ($this->finaldata as $key => $val)
			{
				$this->writeContent($i, $this->formatOutputText($key, $val), $maxstr);
				$i++;
			}
		}
		
		public function gatherFinalData()
		{
			$i = 0;
			foreach ($this->wanted as $val)
			{
				if (isset($this->userdata[$val]))
				{
					if (isset($this->finaldata[$val]))
					{
						$this->finaldata[$val . $i] = $this->userdata[$val];
						$i++;
					}
					else
					{
						$this->finaldata[$val] = $this->userdata[$val];
					}
				}
				else {
					$rand = array_rand($this->userdata);
					$this->finaldata[$rand] = $this->userdata[$rand];
				}
			}
			
			if(sizeof($this->finaldata) < 8)
			{
				while (sizeof($this->finaldata) < 8)
				{
					$rand = array_rand($this->userdata);
					$this->finaldata[$rand] = $this->userdata[$rand];
				}
			}
		}
	
		public function getLenOfLongestVal($arr)
		{
			$maxstr = 0;
			$arrvalues = array_values($arr);
			for ($i = 4; $i < 8; $i++)
			{
				if (strlen($arrvalues[$i]) > $maxstr)
				{
					$maxstr = strlen($arrvalues[$i]);
				}
			}
			return $maxstr;
		}
		
		public function imagettfstroketext(&$image, $size, $angle, $x, $y, &$textcolor, &$strokecolor, $fontfile, $text, $px)
		{
			for($c1 = ($x-abs($px)); $c1 <= ($x+abs($px)); $c1++)
			{
				for($c2 = ($y-abs($px)); $c2 <= ($y+abs($px)); $c2++)
				{
					$bg = imagettftext($image, $size, $angle, $c1, $c2, $strokecolor, $fontfile, $text);
				}
				return imagettftext($image, $size, $angle, $x, $y, $textcolor, $fontfile, $text);
			}
		}
		
		final public function AlphaBlend()
		{
			/* Prequel */
			@imagealphablending($this->Image, true);
			@imagealphablending($this->Background, true);
			@imagealphablending($this->Overlay, true);
			
			/* Sequel */
			@imagesavealpha($this->Image, true);
			@imagesavealpha($this->Background, true);
			@imagesavealpha($this->Overlay, true);
		}
		
		private function writeRequiredContent()
		{
			@$fontwidth = imagefontwidth($this->Font_B);
			$this->Text($this->userdata["lastname"], 8, ((imagesx($this->Overlay) / 2) - $fontwidth * strlen($this->userdata["lastname"]) / 2 - strlen($this->userdata["lastname"])), 10);
			$this->Text((($this->userdata["onlinestatus"] == "true") ? "Online" : "Offline"), 8, 3, 122.5, false, (($this->userdata["onlinestatus"] == "true") ? array(1, 223, 1) : array(255, 0, 0)));
			unset($this->userdata["lastname"], $this->userdata["onlinestatus"]);
			$this->Text("GTIRPG.net", 8, (imagesx($this->Overlay) - 62), 122.5);
		}
		
		private function writeContent($place, $data, $max = 0)
		{
			$startpoint = 27;
			switch ($place)
			{
				case 1:
					$this->Text($data, 9, 25, $startpoint, true, 1);
					break;
				case 2:
					$this->Text($data, 9, 25, ($startpoint + 25), true, array(255, 255, 255), 1);
					break;
				case 3:
					$this->Text($data, 9, 25, ($startpoint + 50), true, 1);
					break;
				case 4:
					$this->Text($data, 9, 25, ($startpoint + 75), true, 1);
					break;
				case 5:
					// was $max*3 - changed on 28/05/14
					$this->Text($data, 9, (imagesy($this->Overlay) +125 - $max*2), $startpoint, true, 1);
					break;
				case 6:
					$this->Text($data, 9, (imagesy($this->Overlay) +125 - $max*2), ($startpoint + 25), true, 1);
					break;
				case 7:
					$this->Text($data, 9, (imagesy($this->Overlay) +125 - $max*2), $startpoint + 50, true, 1);
					break;
				case 8:
					$this->Text($data, 9, (imagesy($this->Overlay) +125 - $max*2), $startpoint + 75, true, 1);
					break;
				default:
					break;

			}
		}

		private function formatOutputText($key, $val = "")
		{
			$dupe = false;
			if (is_numeric(substr($key, -1)))
			{
				$dupe = true;
			}
			
			$data = array("bank_bal" => "Bank",
				"bank" => "Bank",
				"team" => "Team",
				"lastLogin" => "Last Login",
				"lastlogin" => "Last Login",
				"name" => "Account",
				"kills" => "Kills",
				"deaths" => "Deaths",
				"group" => "Group",
				"last_location" => "Location",
				"lastlocation" => "Location",
				"playtime" => "Playtime",
				"kd_ratio" => "K/D",
				"jobrank" => "Job Rank");
			return $data[($dupe ? substr($key, 0, -1) : $key)] . ": " . $val;
		}
		
		public function __destruct()
		{
			$this->AlphaBlend();
			if (isset($this->api))
			{
				ob_start();
					imagepng($this->Image);
					$imagedata = ob_get_contents();
				ob_end_clean();
				echo json_encode(array("image"=>base64_encode($imagedata)));
			}
			else
			{
				if (isset($_GET['dev']))
				{
					header("Content-type: image/png");
					imagepng($this->Image);
				}
				
			}
			imagedestroy($this->Image);
		}
		
	}
?>