<?PHP
class Sigs {
	private $db;
	private $connected;
	public $userdata = array();
	private $inputname;
	
	public function __construct($name)
	{
		$this->connect();
		$this->inputname = $name;
		$this->gatherData();
		$this->formatData();
	}
	
	public function connect()
	{	
		if ($this->connected)
		{
			return;
		}
		
		$con_info = array("127.0.0.1", "gti", "GTI", "X93qT3h3qaRfaLzR");// host, db, user, password
		try
		{
			$this->db = new PDO("mysql:host={$con_info[0]};dbname={$con_info[1]}", $con_info[2], $con_info[3]);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
		
		$this->connected = true;
	}
	
	public function gatherData()
	{
		$this->userdata["bank_bal"] = $this->getBankBalance();
		$this->userdata = $this->userdata + $this->getAccountData();
		$this->userdata = $this->userdata + $this->getStatData();
		echo "<pre>";
		var_dump($this->userdata);
		die();
		/*
		Array ( [bank_bal] => 61217318 [lastname] => LilDolla [onlinestatus] => false [id] => 1 [name] => LilDollaTechZone [kills] => 52 [deaths] => 66 [kd_ratio] => 0.79 )
		ALWAYS DO A print_r($this->userdata); ONCE MODIFIED AND CHANGE THE ABOVE LINE.
		MUST UPDATE THE LINES IN signature.class.php ALSO.
		LAST UPDATED ON: 23:28 / 13/04/2014 GMT(London time)
		*/
	}
	
	public function formatData()
	{
		// Loop through them all, if empty, make it Unknown
		foreach ($this->userdata as $key => $value)
		{
			if ($value == "")
			{
				if ($key == "group")
				{
					$this->userdata[$key] = "Groupless";
				}
				else if ($key == "playtime")
				{
					$this->userdata["playtime"] = 0;
				}
				else
				{
						$this->userdata[$key] = "Unknown";
				}
			}
		}
		
		// Team
		if ($this->userdata["team"] == "Civilian Workforce")
		{
			$this->userdata["team"] = "Civilian";
		}
		// bank bal
		$this->userdata["bank_bal"] = number_format($this->userdata["bank_bal"]);
		$this->userdata["bank_bal"] = "$" . $this->userdata["bank_bal"];
		
		// kd ratio
		$this->userdata["kd_ratio"] = number_format($this->userdata["kills"] / $this->userdata["deaths"], 2);
		
		// Strip hex codes, if any
		$this->userdata["lastname"] = preg_replace("/#[a-f0-9]{6}/i", "", $this->userdata["lastname"]);
		// location
		$split = explode(", ", $this->userdata["last_location"]);
		if (sizeof($split) > 1)
		{
			switch ($split[1])
			{
				case "Los Santos":
					$city = " LS";
					break;
				case "San Fierro":
					$city  = " SF";
					break;
				case "Las Venturas":
					$city = " LV";
					break;
				case "Red County":
					$city = " RC";
					break;
				default:
					$city = " Unknown";
					break;
			}
			$this->userdata["last_location"] = "{$split[0]}, {$city}";
			$name_split = explode(" ", $split[0]);
			if (sizeof($name_split) > 1)
			{
				$name_split_str = "{$name_split[0]} {$name_split[1]}";
			}
			
			if (sizeof($name_split) >= 3)
			{
				switch ($name_split_str)
				{
					case "Los Santos":
						$location = "LS";
						break;
					case "San Fierro":
						$location = "SF";
						break;
					case "Las Venturas":
						$location = "LV";
						break;
					case "Red County":
						$location = "RC";
						break;
					default:
						break;
				}
				
				if (isset($location))
				{
					$this->userdata["last_location"] = "{$location} {$name_split[2]}";
				}
				else
				{
					$this->userdata["last_location"] = implode("", $name_split);
				}
			}
			$this->userdata["lastLogin"] = date("d/m/Y", $this->userdata["lastLogin"]);
		}
	}
	
	public function getBankBalance()
	{

		$q = "SELECT balance FROM banking WHERE name=?";
		$banking = $this->db->prepare($q);

		$banking->bindParam(1, $this->inputname, PDO::PARAM_STR, 12);
		$banking->execute();
		$bal = $banking->fetchColumn(0);
		if (!isset($bal))
		{
			die("Error.");
		}
		return $bal;
	}
	
	public function getAccountData() // return array(lastname => '', onlinestatus => '')
	{
		$q = "SELECT lastname, onlinestatus, team, lastLogin FROM accountdata WHERE name=?";
		/* $q = "SELECT * FROM accountdata WHERE name=?"; */
		$accdata = $this->db->prepare($q);
		$accdata->bindParam(1, $this->inputname, PDO::PARAM_STR, 12);
		$accdata->execute();
		$res = $accdata->fetch(PDO::FETCH_ASSOC);
		return $res;
	}
	
	public function getStatData() // return all account stats ; Array ( [id] => 1 [name] => LilDollaTechZone [kills] => 53 [deaths] => 75 [group] => KLM [last_location] => Santa Flora, San Fierro ) 1
	{
		$qu = "SELECT * FROM stats WHERE name=?";
		$sdata = $this->db->prepare($qu);
		$sdata->bindParam(1, $this->inputname, PDO::PARAM_STR, 12);
		$sdata->execute();
		$res = $sdata->fetch(PDO::FETCH_ASSOC);
		unset($res["id"]);
		return $res;
	}
}
?>