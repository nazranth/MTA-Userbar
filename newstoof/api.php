<?PHP
die();

error_reporting(E_ALL);
ini_set('display_errors', '1');
date_default_timezone_set('Europe/London');


require_once("classes/signature.class.php");
require_once("classes/signature.image.class.php");
require_once("classes/signature.utils.class.php");
$utils = new Utils();
$utils->Init();
$data = new Sigs($_GET['username']);
$data->userdata["from_api"] = true;
$img = new SignatureImg($data->userdata, $utils->wanted);

?>