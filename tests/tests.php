<?php

if (filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) { die(); } // only allow local

require_once("../classes/signature.class.php");
require_once("../classes/signature.image.class.php");

$manipulate = [
	"bank_bal" => 61217318,
	"lastname" => "Nathan",
	"onlinestatus" => false,
	"id" => 1,
	"name" => "Nathan",
	"kills" => 52,
	"deaths" => 66,
	"kd_ratio" => 0.79,
	"last_location" => "SF",
	"group" => "TestGroup",
	"last_login" => date("d/m/Y"),
	"team" => "Criminal"
];
$img = new SignatureImg($manipulate);